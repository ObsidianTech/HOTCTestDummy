﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HOTCTestDummyLibrary.Models;

namespace HOTCTestDummyLibrary.Data
{
    public class ApplicationDBContext : DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Data Source=RAHSACLOVE\\SQLEXPRESS;Initial Catalog=HOTCTestDb;User ID=sa;Password=Obsidian12!");
        }

        public DbSet<Event> Events { get; set; }
    }
}
